<?php

header("Content-Type: image/png");

$image = imagecreate(256, 256);

// Alterar cor, a ordem faz diferença
$black = imagecolorallocate($image, 0, 0, 0);
$red = imagecolorallocate($image, 255, 0, 0);

// Adicionando texto a imagem
// imagestring (image, tamnho-font, posição-x, y, frase, color)
imagestring ($image, 10, 60, 120, "Curso php 7", $red);

// Renderizando imagem
imagepng($image);

// Retirar a imagem da memória
imagedestroy($image);

?>