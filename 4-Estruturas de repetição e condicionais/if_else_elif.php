<?php

$qualasuaidade = 24;

$idadeCrianca = 12;
$idadeMaior = 18;
$idadeMelhor = 65;

//Condicional padrão
if ($qualasuaidade < $idadeCrianca){
    echo "É criança";
} else if ($qualasuaidade < $idadeMaior){
    echo "É adolecete";
} else if ($qualasuaidade < $idadeMelhor){
    echo "É adulto";
} else {
    echo "É idoso";
}

echo "<br>";

//Para condições rápidas
echo ($qualasuaidade < $idadeMaior)?"Menor de idade":"Maior de idade";
?>