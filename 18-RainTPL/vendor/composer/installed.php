<?php return array(
    'root' => array(
        'pretty_version' => 'dev-main',
        'version' => 'dev-main',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => '00b918b41b99ae0c763ec81029821076f9c00495',
        'name' => '__root__',
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => '00b918b41b99ae0c763ec81029821076f9c00495',
            'dev_requirement' => false,
        ),
        'rain/raintpl' => array(
            'pretty_version' => '3.1.1',
            'version' => '3.1.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../rain/raintpl',
            'aliases' => array(),
            'reference' => 'edee683bf242f40cc75bee46a78759f6c1589dca',
            'dev_requirement' => false,
        ),
    ),
);
