<?php

$a = 10;

function trocaValor($a){
    $a += 50;
    return $a;
}
echo "Sem o &<br>";
echo "Variável local: ";
echo trocaValor($a);
echo "<br>" . "Variável global: ";
echo $a;

function trocaValor2(&$a){
    $a += 50;
    return $a;
}
// O & altera a variável no sem enderço de memoria de acordo com a local
echo "<hr>Com o &<br>";
echo "Variável local: ";
echo trocaValor2($a);
echo "<br>" . "Variável global: ";
echo $a;