<?php

echo "Use '__construct' para criar um método mágico<br>Mátodos
mágicos são metodos acionados automáticamente ao se instanciar uma classe<hr>";

class Endereco {

    private $logradouro;
    private $numero;
    private $cidade;

    public function __construct($a, $b, $c){

        $this->logradouro = $a;
        $this->numero = $b;
        $this->cidade = $c;
    }

    // É invocado quando e deletado, desconectado ou se sai da pagina
    public function __destruct(){
        var_dump("DESTRUIR");
    }

    public function __toString(){
        return $this->logradouro.", ".$this->numero." - ".$this->cidade;
    }
}

$meuEndereco = new Endereco("Rua Ademar Saraiva Leão", "123", "Santos");

echo $meuEndereco."<br><br>";

unset($meuEndereco);
?>