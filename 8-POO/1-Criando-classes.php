<?php

class Pessoa {

    // Atributo de classe
    public $nome;

    // Método de classe
    public function falar(){

        // $this puxa o atributo da própria função
        return "O meu nome é ".$this->nome;
    }
}

$glaucio = new Pessoa();
$glaucio->nome = "Glaucio Daniel";
echo $glaucio->falar();
?>