<?php
echo "Polimorfismo é quando metodos iguais em classes diferentes tem comportamentos diferentes<hr><br>";

abstract class Animal{
    public function falar(){
        return "Som";
    }
    public function mover(){
        return "Anda";
    }
}

class Cachorro extends Animal{
    public function falar(){
        return "Late";
    }
}

class Gato extends Animal{
    public function falar(){
        return "Mia";
    }
}

class Passaro extends Animal{
    public function falar(){
        return "Pia";
    }
    public function mover(){
        //parent::mover() acessa mover da classe pai
        return "Voa ou " . parent::mover();
    }
}

$pluto = new Cachorro();
echo "Pluto ";
echo $pluto->falar() . " e ";
echo $pluto->mover() . "<br>";

echo "---------------------------------<br>";

$garfield = new Gato();
echo "Garfield ";
echo $garfield->falar() . " e ";
echo $garfield->mover() . "<br>";

echo "---------------------------------<br>";

$piipiu = new Passaro();
echo "Piipiu ";
echo $piipiu->falar() . ", ";
echo $piipiu->mover() . "<br>";
?>