<?php
echo "Interface obriga as classes inferfaceadas a terem os
atributos e métodos pré-estabelecidos<hr><br>";

interface Veiculo {

    public function acelerar($velocidade);
    public function frenar($velocidade);
    public function trocarMarcha($Marcha);
}

class Civic implements Veiculo{

    
    public function acelerar($velocidade){
        echo "O veículo acelerou até " . $velocidade . " km/h";
    }
    public function frenar($velocidade){
        echo "O veículo frenou até " . $velocidade . " km/h";

    }
    public function trocarMarcha($marcha){
        echo "O veículo engatou a marcha " . $marcha;

    }
}

$carro = new Civic();
$carro->trocarMarcha(1)
?>