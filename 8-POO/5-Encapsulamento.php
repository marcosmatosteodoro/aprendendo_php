<?php

class Pessoa {
    // Qualquer um consegue acesso ao instanciar e classe
    public $nome ="Rasmus Lerdorf";
    // Só tem acesso a esse nivel metodos da propria classe ou classe filha
    protected $idade = 40;
    // Só tem acesso a esse nivel metodos da propria Classe
    private $senha = "123456";

    public function verDados(){
        echo $this->nome . "<br>";
        echo $this->idade . "<br>";
        echo $this->senha . "<br>";
    }
}

class Programador extends Pessoa {

    public function verDados2(){
        echo $this->nome . "<br>";
        echo $this->idade . "<br>";
        // Não tem acesso a senha por ser u método privado da classe anterior
        echo $this->senha . "<br>";
    }
}

$objeto = new Pessoa();
$objeto2 = new Programador();


//echo $objeto->nome . "<br>";
//echo $objeto->idade . "<br>";
//echo $objeto->senha . "<br>";

$objeto->verDados();
echo "<br>";
$objeto2->verDados2();

?>