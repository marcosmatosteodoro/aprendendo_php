<?php

$con = new PDO("mysql:dbname=dbphp7;host=localhost", "root", "");

$stmt = $con->prepare("SELECT * FROM tb_usuarios ORDER BY deslogin");

$stmt->execute();
//Fatia as linhas sem precisar usar while
//PDO::FETCH_ASSOC MANDA só os dados internos sem índices
$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

foreach ($results as $row){
    foreach ($row as $key => $value){
        echo "<strong>". $key . ": </strong>".$value."<br>";
    }
    echo "===================================================<br>";
}

?>