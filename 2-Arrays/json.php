<?php

$pessoas = array();

array_push($pessoas, array(
    'nome'=>'Joao',
    'idade'=>20
));
array_push($pessoas, array(
    'nome'=>'Glaucio',
    'idade'=>25
));
array_push($pessoas, array(
    'nome'=>'Marcos',
    'idade'=>24
));

// Espera um array e transforma em json
echo json_encode($pessoas);

echo "<br>" . "<hr>" . "<br>";

$json = '[{"nome":"Joao","idade":20},{"nome":"Glaucio","idade":25},{"nome":"Marcos","idade":24}]';
//decodifica o json e transforma em array denovo
$data = json_decode($json, true);

var_dump($data)
?>