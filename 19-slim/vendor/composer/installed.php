<?php return array(
    'root' => array(
        'pretty_version' => 'dev-main',
        'version' => 'dev-main',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => '00b918b41b99ae0c763ec81029821076f9c00495',
        'name' => '__root__',
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => '00b918b41b99ae0c763ec81029821076f9c00495',
            'dev_requirement' => false,
        ),
        'slim/slim' => array(
            'pretty_version' => '2.0.0',
            'version' => '2.0.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../slim/slim',
            'aliases' => array(),
            'reference' => '3e95c9abbc57a8f023ab4dacebcab9dae4d9f1f0',
            'dev_requirement' => false,
        ),
    ),
);
