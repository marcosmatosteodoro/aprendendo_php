<p> Para declarar uma variável em php se inicia com $</p>
<p>Exemplo '$nome = "Hcode"'<hr>

<?php
//Variáveis
$nome1 = "Atreus";
$nome2 = "Kratos";
$nomecomp = $nome1 ." " . $nome2;
$frutas = array('Maça','Laranja','Banana','Limão');
$nascimento = 1990;

var_dump($nome1); //Informações da variável

echo "<br>";
echo $nome1 ;
echo " e ";
echo $nome2;
echo " ou ";
echo $nomecomp . "<br>";

//Arrays
echo $frutas[2] . ", " . $frutas[0] . ", " . $frutas[3] . '<br>';

// Orientação e objetos
$nascimento = new DateTime();
var_dump($nascimento);
echo '<br>' . $nascimento;

$nulo = null


exit; //Para de executar daqui pra baixo

unset($nome1); //Variável excluida
echo "<br>";
echo $nome1

/*
comentário de blocos
*/

?>