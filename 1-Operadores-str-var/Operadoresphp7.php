<?php

echo "Operadores do php 7"."<hr>";
// SPACESHIP
$a = 50;
$b = 35;
/*  Se a > b  = 1
    Se a == b = 0
    Se a < b  = -1 
*/
var_dump($a <=> $b);
echo "<br>";
// NULL COLECT 
$c = NULL;
$d = NULL;
$e = NULL;
// Se c for nulo tente d se nulo tente e se nulo tente a
echo $c ?? $d ?? $e ?? $c;
//Incremento
echo "<br>"."Operador de incremento". "<br>";
echo $a++ . " " . $a . "<br>";
echo "Operador de pré-incremento". "<br>";
echo ++$a . " " . $a . "<br>";
//Decremento
echo "<br>"."Operador de decremento". "<br>";
echo $a-- . " " . $a . "<br>";
echo "Operador de pré-decremento". "<br>";
echo --$a . " " . $a . "<br>";
?>